---
title: "Getting Started"
date: 2021-03-07T08:22:44+11:00
draft: false
weight: 10
---

See the information at https://papercutsoftware.gitlab.io/docs-as-code/0080-static-websites.html
on how to set up a simple Hugo website

