#!/usr/bin/env bash

DOCS_PROJECT="demo-docs-project"

source ./demo-magic.sh

DEMO_CMD_COLOR=$CYAN

[[ -d ${DOCS_PROJECT} ]] && rm -rf ${DOCS_PROJECT}

clear

cat <<EOF

Nine Easy Steps to getting a Hugo site running

Step 1. Install Hugo. See https://gohugo.io/getting-started/installing/

Step 2. Create a new Hugo project

EOF

pe "hugo new site ${DOCS_PROJECT}"

echo
read -p "Let's look at what this created."
echo 
pei "tree ${DOCS_PROJECT}"

printf "\nMake our new project directory the default\n\n"

pe "cd ${DOCS_PROJECT}"

echo 

cat <<EOF

Step  3. Install a theme

a. Select a theme at https://themes.gohugo.io/tags/documentation/.

b. Review install and usage instructions

c. Follow install procedure

For example:

EOF

pe "mkdir -p themes/hugo-geekdoc/ && \
wget -qO - https://github.com/thegeeklab/hugo-geekdoc/releases/latest/download/hugo-geekdoc.tar.gz | \
tar -xzf - -C themes/hugo-geekdoc/"

# Note: This example takes the "latest" version, whatever that is. Later we will fix the version when deploying
echo
echo This installed a whole bunch of files in the themes/hugo-geekdoc directory
echo

pe "tree  -L 4"

read x

echo 
echo Step 4. Edit your "config.toml" file to reference the theme e.g.

cp ../config-example/config.toml .

pe "code config.toml"

read x

echo "Basic Hugo configuration is now complete"
echo
read x

echo Step 5. Add simple top level page
echo

pe "hugo new _index.md"
echo
pe "tree -L 2"
echo
echo "Step 6. Now edit our new top page using our fav text editor"

pe "code content/_index.md"
 
 #  **Note**: Your content is located inside the `content` directory.

read x

cat <<EOF

Step 7. Preview the result.

   a. Generate the website static files

   b. Start a local webserver to serve website content.

   Hugo has an option to do both of these steps with a single command. The "server" sub command

   This is a great way to preview changes live.

EOF

pe "hugo server -D  --disableFastRender --bind 0.0.0.0 -v" 

#   * `-D` Generate pages that are marked draft
#   * `serve` Start the Hugo webserver
#   * `--disableFastRender`  Every time a change is made to the content or settings reload it
#   * `--bind "0.0.0.0"` Listen on every network interface so I can test on my cell phone (default is to only respond to requests from `localhost`)

#   More information on command line options [here](https://gohugo.io/commands/hugo/)

cat <<EOF

Step 8. But where can we find our static content to deploy on our production website?

When running Hugo with the "server" option Hugo by default will serve files from memory and avoid writing to disk.

You can force the writing of content by using the --contentDir (-d) option, or don't use the "server" sub command.

EOF

pe "hugo -D -v"

echo Notice Hugo created a directory \'./public\' with the static website content.

pe "tree public"

read x

cat <<EOF

Step 9. Deploy the generated static content to any web server

Using some Docker magic to copy the "public" directory content to an nginx server

(we will gloss over the details)

EOF

pe "docker run --name mynginx -v $PWD/public:/usr/share/nginx/html:ro -it --rm -p 8080:80  nginx"

echo
echo Now we have a local working Hugo environment.
read x
clear
echo

echo "Deploy to a cloud based web server"

read x

cat <<'EOF'

We are going to store our content in the cloud and have a service deploy new website versions when we make changes.

Let's start at step 1

Step 1. Install Git version control tool. See https://git-scm.com/downloads

Step 2. Create a local Git version control database (called a repo)

EOF

pe "git init"


echo
echo We need some content for our web site -- here is some I prepared earlier!
echo
rm  content/*

pe "cp -a ../content-for-demo/* ./content"

echo Let\'s preview the new content

pe "hugo serve -D --disableFastRender --bind 0.0.0.0 -v"

echo
echo "and the files that make our new content"
echo

pe "tree content"

echo Let\'s look at the udpated front matter

pe "code content/getting-started/_index.md"

read x

cp ../config-example/.gitignore .

cat <<'EOF'

Step 3. We need some Hugo specific Git configuration 

Git needs to ignore the the content we don't maintain ourselves

i.e. themes and public

The file .gitignore tells git which content to skip when adding new changes


EOF

pe "cat .gitignore"
echo
echo "Step 4. Add all the uddated content and config files to the local Git repo"
echo

pe "git add ."
pe "git commit -m \"Initial Commmit\""

echo

echo make sure it\'s all worked

echo

pe "git status"

read x

echo
echo "Step 5. Create a project on http://GitLab.com (hint: project name $DOCS_PROJECT)"

read x

echo
echo "Step 6. Connect our local repo to the new cloud repo with some Git magic"
echo
pe "git remote add origin git@gitlab.com:alecthegeek/$DOCS_PROJECT.git"
echo
echo "Step 7. push our local content to the cloud"
echo
pe "git push -u origin --all"

read x
echo
echo Review the content in the GitLab web interface https://gitlab.com/alecthegeek/$DOCS_PROJECT
read x
echo

echo Step 8. Create a Hugo build script for the GitLab CI automation system

echo
echo

cp ../config-example/.gitlab-ci.yml .

pe "code .gitlab-ci.yml"

echo
echo Step 9. Add the new config file to version control and push to the cloud
echo

pe "git add .gitlab-ci.yml"
pe "git commit -m \"Add support for GitLab CI/CD Hugo deployment\""
pe "git push"

echo
echo Let\'s see if it worked!     https://gitlab.com/alecthegeek/${DOCS_PROJECT}/-/jobs

read x
echo
echo Do we have a website?      https://alecthegeek.gitlab.io/${DOCS_PROJECT}/
echo
echo Note these are private pages. We can make them public if we choose
read x
echo
echo "That's all folks!"
